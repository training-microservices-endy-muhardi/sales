package com.artivisi.training.microservices.sales.dao;

import com.artivisi.training.microservices.sales.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
