package com.artivisi.training.microservices.sales.controller;

import com.artivisi.training.microservices.sales.dao.CustomerDao;
import com.artivisi.training.microservices.sales.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired private CustomerDao customerDao;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveNewCustomer(@RequestBody @Valid Customer customer) {
        customerDao.save(customer);
    }

    @GetMapping("/")
    public Page<Customer> findCustomer(Pageable page) {
        return customerDao.findAll(page);
    }
}
